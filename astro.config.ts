import { AstroUserConfig, defineConfig } from "astro/config";
import nodejs from "@astrojs/node";

// https://astro.build/config
const config: AstroUserConfig = {
  output: "server",
  adapter: nodejs({
    mode: "standalone",
  }),
  server: ({ command }) => ({
    port: process.env.NODE_ENV === "production" ? 80 : 3000,
    host: "0.0.0.0",
  }),
  site:
    process.env.NODE_ENV === "production"
      ? "https://lvna.gay"
      : "http://localhost:3000",
};
export default defineConfig(config);

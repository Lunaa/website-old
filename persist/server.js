const express = require("express");
const { readFileSync, writeFileSync, existsSync } = require("fs");

const app = express();
app.use(express.json());

let data = {};

if (existsSync("./data.json")) {
  data = JSON.parse(readFileSync("./data.json", "utf-8"));
}

app.post("/setData", (req, res) => {
  Object.entries(req.body).forEach((i) => {
    console.log(`Data ${i[0]} updated`);
    data[i[0]] = i[1];
  });
  writeFileSync("./data.json", JSON.stringify(data));
  res.sendStatus(200);
});

app.post("/data", (req, res) => {
  const returnObject = {};
  req.body.forEach((i) => {
    returnObject[i] = data[i];
  });
  res.json(data);
});

app.get("/healthCheck", (req, res) => {
  res.sendStatus(200);
  res.end();
});

const port =
  process.env.PORT || process.env.NODE_ENV === "production" ? 80 : 3001;
app.listen(port, () => {
  console.log(`Data persistence server listening on port ${port}`);
});

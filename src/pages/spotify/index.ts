import type { APIRoute } from "astro";
import { getData } from "@api/spotify";

export const prerender = false;

export const get: APIRoute = async function get({ request }) {
  const data = await getData();
  return {
    body: JSON.stringify(data),
  };
};

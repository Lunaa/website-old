import type { APIContext, APIRoute } from "astro";
import { setRefreshToken, headlessStates } from "@api/spotify";
import { Logger } from "@api/logger";

const logger = new Logger("spotify/auth", import.meta.url, true, 0x00ff00);

export const prerender = false;

export const get: APIRoute = async ({ request, redirect }) => {
  const queryParams = new URLSearchParams(new URL(request.url).search);
  const success = await setRefreshToken(queryParams.get("code")!);
  const isHeadless = headlessStates.includes(queryParams.get("state")!);
  if (success) {
    if (isHeadless) {
      logger.log("Headless auth success");
      return new Response(null, { status: 200 });
    }
    logger.log("The token was accepted!");
    return redirect("/");
  } else {
    logger.log(
      `# <@${import.meta.env.DISCORD_OWNER_ID}> The ${
        isHeadless ? " headless" : ""
      }token was **not** accepted!`,
      { color: 0xff0000 }
    );
    if (isHeadless) return new Response(null, { status: 500 });
    return redirect("/");
  }
};

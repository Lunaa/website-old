interface iSpotifyDataPartial {
  userName?: string;
  url?: string;
  avatarUrl?: string;
}
interface iSpotifyData extends iSpotifyDataPartial {
  song: iSong;
}
interface iSong {
  name: string;
  artist?: string;
  artistUrl?: string;
  album: string;
  albumUrl: string;
  image: string;
  playlist?: string;
  playlistUrl?: string;
  time: number;
  duration: number;
}

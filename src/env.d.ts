/// <reference types="astro/client" />
interface ImportMetaEnv {
  readonly SPOTIFY_CLIENT_ID: string;
  readonly SPOTIFY_CLIENT_SECRET: string;
  readonly SPOTIFY_ACCESS_TOKEN: string;
  readonly SPOTIFY_REFRESH_TOKEN: string;
  readonly SPOTIFY_OWNER_ID: string;
  readonly DISCORD_WEBHOOK_URL: string;
  readonly DISCORD_BOT_TOKEN: string;
  readonly DISCORD_OWNER_ID: string;
  readonly ENABLE_PERSIST: boolean;

  // for automatic spotify auth with puppeteer
  readonly SPOTIFY_PASSWORD?: string;

  // for gravatar
  readonly EMAIL: string;
}
interface ImportMeta {
  readonly env: ImportMetaEnv;
}

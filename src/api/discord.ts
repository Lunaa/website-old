import { type APIUser, UserFlags } from "discord-api-types/v10";

export const prerender = false;

interface iDiscordUser {
  username: string;
  displayName: string;
  banner_color: string;
  avatar_url: string;
  flagImages: { [key: string]: string };
  id: string;
}

interface iClientModBadge {
  name: string;
  badge: string;
}

const flagToImage = {
  ActiveDeveloper: "activedeveloper",
  BugHunterLevel1: "bughunterlevel1",
  BugHunterLevel2: "bughunterlevel2",
  CertifiedModerator: "certifiedmod",
  HypeSquadOnlineHouse1: "hypesquadbravery",
  HypeSquadOnlineHouse2: "hypesquadbrilliance",
  HypeSquadOnlineHouse3: "hypesquadbalance",
  Hypesquad: "hypesquadevents",
  Partner: "discordpartner",
  PremiumEarlySupporter: "earlysupporter",
  Staff: "discordstaff",
  VerifiedDeveloper: "earlyverifiedbotdev",
};

export let username = "Loading...";
export let displayName = "Loading...";
export let banner_color = "#27292b";
export let avatar_url =
  "https://canary.discord.com/assets/1f0bfc0865d324c2587920a7d80c609b.png";
export let user_id = "0";
export let flagImages: { [key: string]: string } = {};

await updateData();

async function updateData() {
  const data = await fetch(
    `https://discord.com/api/v10/users/${import.meta.env.DISCORD_OWNER_ID}`,
    {
      headers: {
        Authorization: `Bot ${import.meta.env.DISCORD_BOT_TOKEN}`,
      },
    }
  );
  const json = (await data.json()) as APIUser;
  let DiscordUser: iDiscordUser = {
    username: json.username,
    displayName: json.global_name || json.username,
    banner_color: `#${json.accent_color?.toString(16)}` || "#27292b",
    avatar_url:
      `https://cdn.discordapp.com/avatars/${json.id}/${json.avatar}.png` ||
      "https://canary.discord.com/assets/1f0bfc0865d324c2587920a7d80c609b.png",
    flagImages: {},
    id: json.id,
  };
  if (json.flags) {
    Object.keys(UserFlags)
      .filter((key) => json.flags! & UserFlags[key])
      .map(
        (image) =>
          (DiscordUser.flagImages[
            image
          ] = `https://raw.githubusercontent.com/efeeozc/discord-badges/main/png/${flagToImage[image]}.png`)
      );
  }

  const modBadges = await fetch(
    `https://clientmodbadges-api.herokuapp.com/users/${DiscordUser.id}`
  ).then((res) => res.json());
  Object.entries(modBadges).forEach((data: any) => {
    data[1].forEach((badge: string | iClientModBadge) => {
      if (typeof badge === "string") {
        DiscordUser.flagImages[
          badge
        ] = `https://clientmodbadges-api.herokuapp.com/badges/${data[0]}/${badge}`;
      } else {
        DiscordUser.flagImages[badge.name] = badge.badge;
      }
    });
  });

  username = DiscordUser.username;
  displayName = DiscordUser.displayName;
  banner_color = DiscordUser.banner_color;
  avatar_url = DiscordUser.avatar_url;
  flagImages = DiscordUser.flagImages;
  user_id = DiscordUser.id;
}
await updateData();
setInterval(updateData, 1000 * 60); // 10 minutes

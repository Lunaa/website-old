const persistHost =
  process.env.NODE_ENV === "production" ? "persist" : "localhost:3001";
const enablePersist =
  import.meta.env.ENABLE_PERSIST || process.env.NODE_ENV === "production";

export const prerender = false;
export async function push(data: { [key: string]: any }) {
  if (!enablePersist) return;
  await fetch(`http://${persistHost}/setData`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  });
  return true;
}

export async function pull(variables: string[]) {
  if (!enablePersist) return {};
  const data = await fetch(`http://${persistHost}/data`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(variables),
  });
  return await data.json();
}
